%global _empty_manifest_terminate_build 0
%global gem_name fluentd
Name:		rubygem-fluentd
Version:	1.3.3
Release:	1
Summary:	Fluentd event collector
License:	Apache-2.0
URL:		https://www.fluentd.org/
Source0:	https://rubygems.org/gems/fluentd-1.3.3.gem
BuildArch:	noarch

BuildRequires:	rubygem-bundler
BuildRequires:	rubygem-cool.io
BuildRequires:	rubygem(http_parser.rb)
BuildRequires:	rubygem-msgpack
BuildRequires:	rubygem-serverengine
BuildRequires:	rubygem-sigdump
BuildRequires:	rubygem-strptime
BuildRequires:	rubygem-tzinfo
BuildRequires:	rubygem-tzinfo-data
BuildRequires:	rubygem-webrick
BuildRequires:	rubygem-yajl-ruby
BuildRequires:	ruby
BuildRequires:	ruby-devel
BuildRequires:	rubygems
BuildRequires:	rubygems-devel rsync
BuildRequires:  rubygem(dig_rb) rubygem-rake
Provides:	rubygem-fluentd

%description
Fluentd is an open source data collector designed to scale and simplify log management. It can collect, process and ship many kinds of data in near real-time.

%package help
Summary:	Development documents and examples for fluentd
Provides:	rubygem-fluentd-doc
BuildArch: noarch

%description help
Fluentd is an open source data collector designed to scale and simplify log management. It can collect, process and ship many kinds of data in near real-time.

%prep
%autosetup -n fluentd-1.3.3
gem spec %{SOURCE0} -l --ruby > fluentd.gemspec
sed -i '/http_parser.rb/s/< 0.7.0/<= 0.7.0/g' fluentd.gemspec
sed -i '/tzinfo/s/~> 1.0/>= 1.0/g' fluentd.gemspec
sed -i '/rake/s/~> 11.0/>= 11.0/g' fluentd.gemspec

%build
gem build fluentd.gemspec
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* %{buildroot}%{gem_dir}/
rsync -a --exclude=".*" .%{gem_dir}/* %{buildroot}%{gem_dir}/
if [ -d .%{_bindir} ]; then
	mkdir -p %{buildroot}%{_bindir}
	cp -a .%{_bindir}/* %{buildroot}%{_bindir}/
fi
if [ -d ext ]; then
	mkdir -p %{buildroot}%{gem_extdir_mri}/%{gem_name}
	if [ -d .%{gem_extdir_mri}/%{gem_name} ]; then
		cp -a .%{gem_extdir_mri}/%{gem_name}/*.so %{buildroot}%{gem_extdir_mri}/%{gem_name}
	else
		cp -a .%{gem_extdir_mri}/*.so %{buildroot}%{gem_extdir_mri}/%{gem_name}
fi
	cp -a .%{gem_extdir_mri}/gem.build_complete %{buildroot}%{gem_extdir_mri}/
	rm -rf %{buildroot}%{gem_instdir}/ext/
fi
rm -rf %{buildroot}%{gem_instdir}/{.github,.gitignore,.travis.yml}
pushd %{buildroot}
touch filelist.lst
if [ -d %{buildroot}%{_bindir} ]; then
	find .%{_bindir} -type f -printf "/%h/%f\n" >> filelist.lst
fi
popd
mv %{buildroot}/filelist.lst .

%files -n rubygem-fluentd -f filelist.lst
%dir %{gem_instdir}
%{gem_instdir}/*
%exclude %{gem_cache}
%{gem_spec}

%files help
%{gem_docdir}/*

%changelog
* Thu Jul 29 2021 wutao <wutao61@huawei.com> - 1.3.3-1
- Package init
